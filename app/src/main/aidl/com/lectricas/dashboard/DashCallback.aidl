// DashCallback.aidl
package com.lectricas.dashboard;

interface DashCallback {
    oneway void sendSpeedometerData (in float data);
    oneway void sendTachometerData (in float data);
}
