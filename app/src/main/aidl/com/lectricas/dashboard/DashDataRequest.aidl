// DashDataRequest.aidl
package com.lectricas.dashboard;
import com.lectricas.dashboard.DashCallback;

interface DashDataRequest {
    oneway void requestDashUpdates (in DashCallback callback);
}
