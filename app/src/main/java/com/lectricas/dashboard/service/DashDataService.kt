package com.lectricas.dashboard.service

import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.IBinder
import android.os.RemoteException
import android.os.SystemClock
import com.lectricas.dashboard.DashCallback
import com.lectricas.dashboard.DashDataRequest.Stub
import kotlin.math.sin

class DashDataService : Service() {

    companion object {
        fun makeIntent(context: Context?): Intent {
            return Intent(context, DashDataService::class.java)
        }

        fun generator(time: Float): Float {
            return ((sin(2 * time + Math.PI) + sin(time) + 2) * 25).toFloat()
        }
    }

    private var runUpdates = false

    private val dashDataRequest: Stub = object : Stub() {
        override fun requestDashUpdates(callback: DashCallback) {
            val start = System.currentTimeMillis()
            while (runUpdates) {
                val time = ((System.currentTimeMillis() - start) / 1000f)
                val value = generator(time)
                callback.sendSpeedometerData(value)
                callback.sendTachometerData(value)
                SystemClock.sleep(16)
            }
        }
    }

    override fun onBind(intent: Intent): IBinder? {
        runUpdates = true
        return dashDataRequest
    }

    override fun onUnbind(intent: Intent?): Boolean {
        runUpdates = false
        return super.onUnbind(intent)
    }
}