import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.lectricas.dashboard.viewpager.TwoFingerPagerAdapter

class DashboardViewPagerAdapter(private val context: Context, private val gaugeIds: List<Int>) : TwoFingerPagerAdapter() {
    override fun instantiateItem(collection: ViewGroup, position: Int): View {
        val inflater = LayoutInflater.from(context)
        val layout = inflater.inflate(gaugeIds[position], collection, false)
        collection.addView(layout)

        return layout
    }

    override fun destroyItem(collection: ViewGroup, position: Int, view: Any) {
        collection.removeView(view as View)
    }

    override fun getCount() = gaugeIds.size


    override fun isViewFromObject(view: View, any: Any): Boolean {
        return view === any
    }
}