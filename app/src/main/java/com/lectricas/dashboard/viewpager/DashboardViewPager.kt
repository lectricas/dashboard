package com.lectricas.dashboard.viewpager

import android.content.Context
import android.util.AttributeSet
import android.view.ViewGroup
import com.lectricas.dashboard.GaugeView

class DashboardViewPager : TwoFingerViewPager {

    companion object {
        const val SPEEDOMETER = 0
        const val TACHOMETER = 1
    }

    constructor(context: Context?) : super(context!!) {}
    constructor(context: Context?, attrs: AttributeSet?) : super(context!!, attrs) {}

    fun getSpeedometerView(): GaugeView {
        return (getChildAt(SPEEDOMETER) as ViewGroup).getChildAt(0) as GaugeView
    }

    fun getTachometerView(): GaugeView {
        return (getChildAt(TACHOMETER) as ViewGroup).getChildAt(0) as GaugeView
    }
}