package com.lectricas.dashboard

import DashboardViewPagerAdapter
import android.content.ComponentName
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.lectricas.dashboard.service.DashDataService
import com.lectricas.dashboard.viewpager.DashboardViewPager
import com.lectricas.dashboard.viewpager.DepthPageTransformer
import kotlinx.android.synthetic.main.activity_main.dashboardViewPager

class MainActivity : AppCompatActivity() {

    lateinit var dashboardViewPagerAdapter: DashboardViewPagerAdapter

    private val dashConnection: ServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            DashDataRequest.Stub
                .asInterface(service)
                .requestDashUpdates(dashDataCallback)
        }

        override fun onServiceDisconnected(name: ComponentName) {}
    }

    private val dashDataCallback: DashCallback = object : DashCallback.Stub() {
        override fun sendSpeedometerData(data: Float) {
            runOnUiThread {
                if (dashboardViewPager.currentItem == DashboardViewPager.SPEEDOMETER) {
                    dashboardViewPager.getSpeedometerView().updateSpeed(data)
                }
            }
        }

        override fun sendTachometerData(data: Float) {
            runOnUiThread {
                if (dashboardViewPager.currentItem == DashboardViewPager.TACHOMETER) {
                    dashboardViewPager.getTachometerView().updateSpeed(data)
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        dashboardViewPagerAdapter = DashboardViewPagerAdapter(
            this,
            listOf(
                R.layout.item_speedometer,
                R.layout.item_tachometer
            )
        )

        dashboardViewPager.adapter = dashboardViewPagerAdapter
        dashboardViewPager.setPageTransformer(true, DepthPageTransformer())
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        if (hasFocus) hideSystemUI()
    }

    private fun hideSystemUI() {
        window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_FULLSCREEN)
    }

    override fun onStart() {
        super.onStart()
        bindService(DashDataService.makeIntent(this), dashConnection, BIND_AUTO_CREATE)
    }

    override fun onStop() {
        super.onStop()
        unbindService(dashConnection)
    }
}
