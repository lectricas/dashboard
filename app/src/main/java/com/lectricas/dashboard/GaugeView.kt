package com.lectricas.dashboard

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.PorterDuff
import android.graphics.Rect
import android.graphics.RectF
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.View
import androidx.core.content.res.getColorOrThrow
import androidx.core.content.res.getDimensionOrThrow
import androidx.core.content.res.getResourceIdOrThrow
import kotlin.math.cos
import kotlin.math.sin

class GaugeView : View {
    companion object {
        private const val GAUGE_LENGTH = 270 //degree
        private const val ANGLE_START = -135
    }

    private val gapBetweenIndexLines: Float

    private val indexLineWidth: Float
    private val indexLineHeight: Float
    private val indexNumberMargin: Float
    private val needleEndMargin: Float
    private val numbersTextSize: Float
    private val needleLineWidth: Float

    private val numbersPaint = Paint()
    private val indexLinesPaint = Paint()
    private val gaugePaint = Paint()
    private val needlePaint = Paint()
    private val needleDrawable: Drawable

    private val gaugeNumbers: List<Int>
    private val speedRectF = RectF()
    private val textBounds = Rect()

    private var needleAngle = 0f

    private lateinit var staticCanvas: Canvas
    private lateinit var staticBitmap: Bitmap
    private lateinit var needleCanvas: Canvas
    private lateinit var needleBitmap: Bitmap

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        val a = context.obtainStyledAttributes(attrs, R.styleable.GaugeView)
        indexLineWidth = a.getDimensionOrThrow(R.styleable.GaugeView_index_line_width)
        indexLineHeight = a.getDimensionOrThrow(R.styleable.GaugeView_index_line_height)
        indexNumberMargin = a.getDimensionOrThrow(R.styleable.GaugeView_index_number_margin)
        numbersTextSize = a.getDimensionOrThrow(R.styleable.GaugeView_index_number_text_size)
        gaugeNumbers = resources.getIntArray(a.getResourceIdOrThrow(R.styleable.GaugeView_gauge_array_data)).toList()
        needleEndMargin = a.getDimensionOrThrow(R.styleable.GaugeView_needle_end_margin)
        needleLineWidth = a.getDimensionOrThrow(R.styleable.GaugeView_needle_width)
        numbersPaint.apply {
            color = a.getColorOrThrow(R.styleable.GaugeView_numbers_text_color)
            style = Paint.Style.FILL
            textSize = numbersTextSize
        }
        indexLinesPaint.apply {
            strokeWidth = indexLineWidth
            color = a.getColorOrThrow(R.styleable.GaugeView_index_lines_color)
            style = Paint.Style.STROKE
        }
        gaugePaint.apply {
            color = a.getColorOrThrow(R.styleable.GaugeView_gauge_color)
            style = Paint.Style.FILL
        }
        needlePaint.apply {
            color = a.getColorOrThrow(R.styleable.GaugeView_needle_color)
            style = Paint.Style.FILL
        }

        a.recycle()
        gapBetweenIndexLines = GAUGE_LENGTH / (gaugeNumbers.size - 1).toFloat()
        needleDrawable = resources.getDrawable(R.drawable.image_needle, null)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        val width = measuredWidth
        val height = measuredHeight
        val dimen = if (width > height) height else width
        setMeasuredDimension(dimen, dimen)
    }

    @SuppressLint("DrawAllocation")
    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)
        staticBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        staticCanvas = Canvas(staticBitmap)
        needleBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        needleCanvas = Canvas(needleBitmap)
        speedRectF.set(width / 2f - height / 2f, 0f, width / 2f + height / 2f, height.toFloat())
        needleDrawable.setBounds(
            speedRectF.centerX().toInt() - needleLineWidth.toInt() / 2,
            speedRectF.centerY().toInt(),
            speedRectF.centerX().toInt() + needleLineWidth.toInt() / 2,
            speedRectF.bottom.toInt() - indexLineHeight.toInt()
        )

        drawStaticOnCache()
    }

    override fun onDraw(canvas: Canvas) {
        canvas.drawBitmap(staticBitmap, 0f, 0f, needlePaint)
        canvas.drawBitmap(needleBitmap, 0f, 0f, needlePaint)
    }

    private fun drawStaticOnCache() {
        staticCanvas.drawCircle(speedRectF.centerX(), speedRectF.centerY(), speedRectF.width() / 2, gaugePaint)
        staticCanvas.drawCircle(speedRectF.centerX(), speedRectF.centerY(), speedRectF.width() / 2 - indexLineWidth / 2, indexLinesPaint)
        gaugeNumbers.forEachIndexed { index, number ->
            val angle = -1 * index * gapBetweenIndexLines + ANGLE_START
            val text = number.toString()
            numbersPaint.getTextBounds(text, 0, text.length, textBounds)

            val sine = sin(angle.toRadians())
            val cosine = cos(angle.toRadians())

            //draw index lines
            val xLineStart = (width / 2 + cosine * (speedRectF.width() / 2))
            val yLineStart = (height / 2 - sine * (speedRectF.width() / 2))
            val xLineEnd = width / 2 + cosine * (speedRectF.width() / 2 - indexLineHeight)
            val yLineEnd = height / 2 - sine * (speedRectF.width() / 2 - indexLineHeight)
            staticCanvas.drawLine(xLineEnd, yLineEnd, xLineStart, yLineStart, indexLinesPaint)
            //end draw index lines

            //draw numbers with margins
            val xNumber = width / 2 + cosine * (speedRectF.width() / 2 - indexLineHeight - indexNumberMargin)
            val yNumber = height / 2 - sine * (speedRectF.width() / 2 - indexLineHeight - indexNumberMargin)
            val shiftX = (cosine + 1) / 2 * textBounds.width()
            val shiftY = (sine + 1) / 2 * textBounds.height()
            val xRTemp = xNumber - shiftX
            val yRTemp = yNumber + shiftY
            staticCanvas.drawText(text, xRTemp, yRTemp, numbersPaint)
            //end draw numbers
        }
    }

    private fun drawNeedle() {
        needleCanvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR)
        needleCanvas.save()
        needleCanvas.rotate(needleAngle, speedRectF.centerX(), speedRectF.centerY())
        needleDrawable.draw(needleCanvas)
        needleCanvas.restore()
        invalidate()
    }

    fun updateSpeed(progress: Float) {
        needleAngle = 45 + progress * GAUGE_LENGTH / 100f
        drawNeedle()
    }
}

fun Float.toRadians(): Float {
    return (this / 180.0 * Math.PI).toFloat()
}